//https://leetcode.com/problems/search-a-2d-matrix/description/

class Solution 
    {
    func searchMatrix(_ matrix: [[Int]], _ target: Int) -> Bool 
        {
        var rows = matrix.count;
        if(rows == 0)
            {
            return false;
            }
        var cols = matrix[0].count;
        if(cols == 0)
            {
            return false;
            }
        var index = -1;
        var start = 0;
        var mid = 0;
        var end = rows - 1;
        var found = false;
        while(start <= end && rows > 1)                                        //Find the potential row
            {
            mid = (start + end) / 2; 
            if(target >= matrix[mid][0] && target <= matrix[mid][cols-1])
                {
                index = mid;
                break;
                }      
            else if(target > matrix[mid][0])
                {
                start = mid + 1;
                }
            else 
                {
                end = mid - 1;
                }
            }
        if(rows == 1)
            {
            index = 0;
            }
        else if(index == -1)
            {
            return false;
            }
        start = 0;
        end = cols - 1;
        while(start <= end && cols > 1)                                    //Search in selected row
            {
            mid = (end + start) / 2;
            if(target == matrix[index][mid])
                {
                found = true;
                break;
                }
            else if(target > matrix[index][mid])
                {
                start = mid + 1;
                }
            else 
                {
                end = mid - 1;
                }

            }
        if(cols == 1)
            {
            return matrix[index][0] == target;
            }
        return found;
        }
    }