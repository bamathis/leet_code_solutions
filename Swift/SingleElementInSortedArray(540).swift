//https://leetcode.com/problems/single-element-in-a-sorted-array/description/

class Solution 
    {
    func singleNonDuplicate(_ nums: [Int]) -> Int 
        {
        var length = nums.count;
        var start = 0;
        var end = length;
        var mid = (length - 1)/2;
        var adjust = 0;
        var done = false;
        if(mid == 0)
            {
            done = true;    
            }
        var number = nums[mid];
        while(!done)
            {
            adjust = ((end - start) / 2) % 2;
            mid = start + (end - start) / 2;
            if(mid - 1 >= 0 && nums[mid] == nums[mid-1])
                {
                if(adjust == 0)
                    {
                    end = mid;
                    }
                else
                    {
                    start = mid + 1;
                    }
                } 
             else if(mid + 1 < length && nums[mid] == nums[mid+1])
                {
                if(adjust == 0)
                    {
                    start = mid;
                    }
                else
                    {
                    end = mid - 1;
                    }
                }
            else
                {
                number = nums[mid];
                done = true;
                }
            }
        return number;
        }
    }