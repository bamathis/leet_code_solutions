//https://leetcode.com/problems/powx-n/description/

class Solution 
    {
    func myPow(_ x: Double, _ n: Int) -> Double 
        {
        var value = 1.0;
        var newValue = x;
        var nextPower = 0;
        var previousPower = 0;
        var negative = false;
        var m = n;
        var power = 0;
        if(n == 0)
            {
            return 1;
            }
        else if(n == 1)
            {
            return x;    
            }
        else if(n < 0)
            {
            negative = true;
            m = n * -1;
            }
        else if(n % 2 == 1)
            {
            value *= x;
            m = m - 1;
            }
        while(power < m)
            {
            nextPower = 2;
            previousPower = 1;
            newValue = x;
            while(nextPower <= m)
                {
                newValue *= newValue;
                previousPower = nextPower;
                nextPower *= 2;
                }
            power *= previousPower;
            value *= newValue;
            m -= previousPower;
            }
        if(negative)
            {
            value = 1 / value;    
            }
        return value;
        }
    }