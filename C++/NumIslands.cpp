//https://leetcode.com/problems/number-of-islands/description/

class Solution 
    {
    public:
        int numIslands(vector<vector<char>>& grid) 
            {
            int islands = 0;
            vector<vector<bool> > checkedArray(grid.size()); 
            for (int i = 0; i < grid.size(); i++) 
                {
                checkedArray[i] = vector<bool>(grid[i].size());
                for (int j = 0; j < grid[i].size(); j++)
                    checkedArray[i][j] = false;
                }

            for(int i = 0; i < grid.size(); i++)
                {
                for(int j = 0; j < grid[i].size(); j++)	
                    {
                    if (!checkedArray[i][j] && grid[i][j] == '1')	
                        {
                        processIsland(grid,checkedArray,j,i);
                        islands++;
                        }
                    }
                }
            return islands;
            }
    
    void processIsland(vector<vector<char>>& grid, vector<vector<bool>>& checkedArray,int column, int row)
        {
        checkedArray[row][column] = true;		
        if (column >= 0 && column + 1 < grid[row].size() && !checkedArray[row][column + 1] && grid[row][column + 1]!= '0') 
            processIsland (grid, checkedArray,column + 1,row);
        if (row >= 0 && row + 1 < grid.size() && !checkedArray[row + 1 ][column] && grid[row + 1][column]!= '0')  
            processIsland(grid, checkedArray, column, row + 1);
        if (column > 0 && !checkedArray[row][column - 1] && grid[row][column - 1]!= '0') 
            processIsland(grid, checkedArray, column - 1 ,row);
        if(row > 0  && !checkedArray[row - 1][column] && grid[row - 1][column]!= '0')  
            processIsland(grid, checkedArray, column, row - 1);
        }

    };