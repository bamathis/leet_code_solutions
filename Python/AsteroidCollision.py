#https://leetcode.com/problems/asteroid-collision/description/
class Solution:
    def asteroidCollision(self, asteroids):
        """
        :type asteroids: List[int]
        :rtype: List[int]
        """
        output = []
        length = len(asteroids)
        for i in range (0,length):
            output.append(asteroids[i])
            j = len(output) - 1
            while(j >= 1 and output[j - 1] > 0 and output[j] < 0):
                if(abs(output[j]) < abs(output[j - 1])):
                    output.pop(j)
                    j = -1
                elif (abs(output[j]) > abs(output[j - 1])):
                    output[j - 1] = output[j]
                    output.pop(j)
                    j = j - 1
                else:
                    output.pop(j)
                    output.pop(j - 1)
                    j = -1

        return output