#https://leetcode.com/problems/integer-to-english-words/description/
class Solution:
    def numberToWords(self, num):
        """
        :type num: int
        :rtype: str
        """
        place = ["", "Thousand","Million", "Billion"]
        ones = ["Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine"]
        teens = ["Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"]
        tens = ["Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"]
        if(num == 0):
            return ones[0]
        placeCount = 0
        inputNumberStr = str(num)
        finalNumberString = ""
        
        while(len(inputNumberStr) > 0):
            sectionOutput = ""
            section = inputNumberStr
            if(len(section) > 3):
                section = section[len(section) - 3:]
                inputNumberStr = inputNumberStr[:len(inputNumberStr) - 3]
            else:
                inputNumberStr = ""
            currentSectionValue = int(section)
            while(currentSectionValue > 0):
                if(currentSectionValue >= 100):
                    currentDigit = int(currentSectionValue / 100)
                    currentSectionValue = currentSectionValue % 100
                    sectionOutput = ones[currentDigit] + " Hundred"
                elif(currentSectionValue >= 10):
                    if(currentSectionValue < 20):
                        currentDigit = currentSectionValue % 10
                        currentSectionValue = 0
                        if(len(sectionOutput) != 0):
                            sectionOutput = sectionOutput + " "
                        sectionOutput = sectionOutput + teens[currentDigit]
                    else:
                        currentDigit = int(currentSectionValue / 10) - 2
                        currentSectionValue = currentSectionValue % 10
                        if(len(sectionOutput) != 0):
                            sectionOutput = sectionOutput + " "
                        sectionOutput = sectionOutput + tens[currentDigit]
                else:
                    currentDigit = currentSectionValue
                    currentSectionValue = 0
                    if(len(sectionOutput) != 0):
                        sectionOutput = sectionOutput + " "
                    sectionOutput = sectionOutput + ones[currentDigit]
            if(placeCount > 0 and sectionOutput != ""):
                finalNumberString = sectionOutput + " " + place[placeCount] + " " + finalNumberString
            elif(sectionOutput != ""):
                finalNumberString = sectionOutput + finalNumberString
            placeCount = placeCount + 1
        
        if(finalNumberString[len(finalNumberString)-1] == " "):
            return finalNumberString[:len(finalNumberString)-1]
        return finalNumberString