#https://leetcode.com/problems/set-matrix-zeroes/description/
class Solution:
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        columns = []
        rows = []
        for i in range(0, len(matrix)):
            addRow = 0
            for j in range(0, len(matrix[i])):
                if matrix[i][j] == 0:
                    columns.append(j)
                    addRow = 1
            if addRow == 1:
                rows.append(i)
        
        for i in columns:
            for j in range (0, len(matrix)):
                matrix[j][i] = 0
  
        for i in rows:
            for j in range (0, len(matrix[i])):
                matrix[i][j] = 0
        