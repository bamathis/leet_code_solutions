#https://leetcode.com/problems/spiral-matrix/
class Solution:
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        
        output = []
        shift = False
        rowStart = 0
        rowEnd = len(matrix) - 1
        if rowStart > rowEnd:
            return []
        colStart = 0
        colEnd = len(matrix[0]) - 1
        if colStart >= colEnd:
            for i in range(rowEnd + 1):
                output.append(matrix[i][0])
            return output
        adjust = 0
        if len(matrix) % 2  == 1:
            adjust = 1
        while rowStart < rowEnd and colStart <= colEnd:
            for i in range(colStart,colEnd):
                output.append(matrix[rowStart][i])
            for i in range(rowStart,rowEnd):
                output.append(matrix[i][colEnd])
            for i in range(colEnd - colStart):
                output.append(matrix[rowEnd][colEnd - i])
            if colEnd == colStart:
                rowStart = rowStart + adjust
            for i in range(rowEnd - rowStart):
                output.append(matrix[rowEnd-i][colStart])
            rowStart = rowStart + 1
            rowEnd = rowEnd - 1
            colStart = colStart + 1
            colEnd = colEnd - 1
            
        if len(matrix) % 2 == 1:
            colEnd = colEnd + 1
            row = int(len(matrix)/2)
            for i in range(colStart,colEnd):
                output.append(matrix[row][i])
        
        return output