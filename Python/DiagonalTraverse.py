#https://leetcode.com/problems/diagonal-traverse/description/
class Solution:
    def findDiagonalOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        output = []
        forward = True
        row = len(matrix)
        if(row == 0):
            return output
        col = len(matrix[0])
        j = 0;
        i = 0
        count = 2 * max(row,col) - 1
        while(count > 0): 
            count = count - 1
            if(forward):
                while(i >= 0 and j < col):
                    output.append(matrix[i][j])
                    j = j + 1
                    i = i - 1
                forward = False
                i = i + 1
                if(j == col):
                    j = col - 1
                    i = i + 1
            else:
                while(j >= 0 and i < row):
                    output.append(matrix[i][j])
                    j = j - 1
                    i = i + 1
                forward = True
                j = j + 1
                if(i == row):
                    i = row - 1
                    j = j + 1
        return output
                
                