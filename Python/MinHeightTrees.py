#https://leetcode.com/problems/minimum-height-trees/description/
class Solution:
    def findMinHeightTrees(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        if n <= 1:
            return [0]
        edgeCounts = [0] * n
        graph = {}
        for edge in edges:
            edgeCounts[edge[1]] += 1
            edgeCounts[edge[0]] += 1
            if edge[1] in graph.keys():
                graph[edge[1]].append(edge[0])
            else:
                graph[edge[1]] = [edge[0]]
            if edge[0] in graph.keys():
                graph[edge[0]].append(edge[1])
            else:
                graph[edge[0]] = [edge[1]]
        queue = []
        for node in range(n):
            if edgeCounts[node] == 1:
                queue.append(node)
            
        while queue:
            temp = []
            for node in queue:
                for pathNode in graph[node]:
                    edgeCounts[pathNode] -= 1
                    if edgeCounts[pathNode] == 1:
                        temp.append(pathNode)
            if temp:
                queue = temp
            else:
                return queue
        return queue  