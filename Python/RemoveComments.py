#https://leetcode.com/problems/remove-comments/description/
class Solution:
    def removeComments(self, source):
        """
        :type source: List[str]
        :rtype: List[str]
        """
        output = []
        i = 0
        rows = len(source)
        while i < rows:
            columns = len(source[i])
            currentString = ""
            j = 0
            while j < columns:
                if j + 1 < columns and source[i][j] == '/' and source[i][j + 1] == '/':
                    while j + 2 < columns and source[i][j+2] == '/':
                        j = j + 1
                if j + 1 < columns and source[i][j] == '/' and source[i][j + 1] == '*':
                    j = j + 2
                    if j + 1 >= columns:
                        i = i + 1
                        j = 0
                        columns = len(source[i])
                        while columns == 0:
                            i = i + 1
                            if i >= rows:
                                break
                            columns = len(source[i])
                    while source[i][j] != '*' or source[i][j+1] != '/':
                        j = j + 1
                        if j + 1 >= columns:
                            i = i + 1
                            j = 0
                            columns = len(source[i])
                            while columns == 0:
                                i = i + 1
                                if i >= rows:
                                    break
                                columns = len(source[i])
                    j = j + 1
                elif j + 1 < columns and source[i][j] == '/' and source[i][j + 1] == '/':
                    i = i + 1
                    j = -1
                    if i >= rows:
                        break
                    columns = len(source[i])
                    if len(currentString) != 0:
                        output.append(currentString)
                    currentString = ""
                else:
                    currentString = currentString + source[i][j]
                j = j + 1
            if len(currentString) != 0:
                output.append(currentString)
            i = i + 1
        return output