#https://leetcode.com/problems/sort-colors/description/
class Solution:
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        length = len(nums)
        redIndex = 0
        blueIndex = length - 1
        index = 0
        while index < length:
            if(nums[index] == 0 and redIndex <= index):
                temp = nums[redIndex]
                nums[redIndex]=nums[index]
                nums[index]=temp
                redIndex = redIndex + 1
            elif(nums[index] == 2 and blueIndex >= index):
                temp = nums[blueIndex]
                nums[blueIndex]=nums[index]
                nums[index]=temp
                blueIndex = blueIndex - 1
            else:
                index = index + 1
        