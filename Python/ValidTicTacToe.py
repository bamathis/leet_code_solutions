#https://leetcode.com/problems/valid-tic-tac-toe-state/description/
class Solution:
    def validTicTacToe(self, board):
        """
        :type board: List[str]
        :rtype: bool
        """
        xCount = 0
        oCount = 0
        xCounts = []
        self.dimensions = 3
        for i in range(self.dimensions):
            for j in range(self.dimensions):
                if board[i][j] == 'X':
                    xCount = xCount + 1                
                elif board[i][j] == 'O':
                    oCount = oCount + 1       
        if xCount < oCount or xCount > oCount + 1:
            return False
        
        xWin = self.didWin('X',board)
        oWin = self.didWin('O',board)
        if xWin and oWin:
            return False
        elif xWin and not oWin:
            return not xCount == oCount 
        elif not xWin and oWin:
            return xCount == oCount
        else:
            return True

    def didWin(self,letter,board):
        counts = [0,0]
        for i in range(self.dimensions):
            counts[0] = 0
            counts[1] = 0
            for j in range(self.dimensions):
                if board[i][j] == letter:
                    counts[0] = counts[0] + 1
                if board[j][i] == letter:
                    counts[1] = counts[1] + 1
            if max(counts) == self.dimensions:
                return True
        counts[0] = 0
        counts[1] = 0
        for i in range(self.dimensions):
            if board[i][i] == letter:
                counts[0] = counts[0] + 1
            if board[i][self.dimensions-1-i] == letter:
                counts[1] = counts[1] + 1
        if max(counts) == self.dimensions:
            return True
        return False         
                
        