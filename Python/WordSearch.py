#https://leetcode.com/problems/word-search/description/class Solution(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        self.rows = len(board)
        self.cols = len(board[0])
        if len(word) > self.rows * self.cols:
            return False
        indices = {}
        for i in range(self.rows):
            for j in range(self.cols):
                if board[i][j] in indices:
                    indices[board[i][j]].append([i,j])
                else:
                    indices[board[i][j]] = [[i,j]]
        unique = list(set(word))
        for letter in unique:
            if letter not in indices:
                return False
        return self.searchWord(indices, word, [], None)
    
    def searchWord(self, indices, word, usedList, previousPos):
        if not word:
            return True
        for index in indices[word[0]]:
            if previousPos == None or (self.isValidMove(index, previousPos) and index not in usedList):
                usedList.append(index)
                if self.searchWord(indices, word[1:], usedList, index):
                    return True
                usedList.remove(index)
        return False

    def isValidMove(self,current,previous):
        return abs(current[0]-previous[0]) + abs(current[1]-previous[1])  == 1