#https://leetcode.com/problems/range-sum-query-2d-immutable/description/
import numpy as np
class NumMatrix:

    def __init__(self, matrix):
        """
        :type matrix: List[List[int]]
        """
        
        self.rows = len(matrix)
        if len(matrix) > 0:
            self.cols = len(matrix[0])
        else:
            self.cols = -1
            return
        
        self.matrix = []             
        self.matrix.append([0] * self.cols)
        self.matrix[0][0] = matrix[0][0]  
        for i in range(1,self.rows):  
            self.matrix.append([0] * self.cols)
            self.matrix[i][0] = matrix[i][0] + self.matrix[i-1][0]
            
        for i in range(1,self.cols):
            self.matrix[0][i] = matrix[0][i] + self.matrix[0][i-1]
        
        for i in range(1,self.rows):
            for j in range(1,self.cols):
                self.matrix[i][j] = matrix[i][j] + self.matrix[i][j - 1] + self.matrix[i - 1][j] - self.matrix[i - 1][j - 1]
        return None

    def sumRegion(self, row1, col1, row2, col2):
        """
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        if self.cols == -1:
            return 0
        if row1 != 0 and col1 != 0:
            return self.matrix[row2][col2] - self.matrix[row2][col1 - 1] - self.matrix[row1 - 1][col2] + self.matrix[row1 - 1][col1 - 1];
        elif row1 == 0 and col1 != 0:
            return self.matrix[row2][col2] - self.matrix[row2][col1 - 1]
        elif row1 != 0 and col1 == 0:
            return self.matrix[row2][col2] - self.matrix[row1 - 1][col2]  
        else:
            return self.matrix[row2][col2]


# Your NumMatrix object will be instantiated and called as such:
# obj = NumMatrix(matrix)
# param_1 = obj.sumRegion(row1,col1,row2,col2)