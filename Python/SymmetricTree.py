# https://leetcode.com/problems/symmetric-tree/description/

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        queue = []
        if(root is None):
            return True
        elif(root.left == None and root.right == None):
            return True
        elif(root.left == None or root.right == None):
            return False
        elif(root.left.val != root.right.val):
            return False
        queue.append(root.left)
        queue.append(root.right)
        while(len(queue) != 0):
            left = queue.pop(0)
            right = queue.pop(0)
            if(left.left != None and right.right != None):
                if(left.left.val != right.right.val):
                    return False
                queue.append(left.left)
                queue.append(right.right)
            elif((left.left != None and right.right == None) or (left.left == None and right.right != None)):
                return False
            if(left.right != None and right.left != None):
                if(left.right.val != right.left.val):
                    return False
                queue.append(left.right)
                queue.append(right.left)
            elif((left.right != None and right.left == None) or (left.right == None and right.left != None)):
                return False
                
        return True