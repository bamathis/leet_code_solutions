#https://leetcode.com/problems/maximum-swap/description/
class Solution:
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int
        """
        numString = list(str(num))
        length = len(numString)
        i = 0
        j = 0
        searching = True
        while(searching and i < length):
            j = i + 1
            maxPos = i            
            while(j < length):
                if(numString[maxPos] <= numString[j]):
                    maxPos = j
                j = j + 1
            if(maxPos != i and numString[maxPos] != numString[i]):
                temp = numString[i]
                numString[i] = numString[maxPos]
                numString[maxPos] = temp
                searching = False
                
            i = i + 1
            
        return int(''.join(numString))