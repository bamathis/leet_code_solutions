#https://leetcode.com/problems/edit-distance/description/
class Solution:
    def minDistance(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        output = [] 
        length1 = len(word1)
        length2 = len(word2)
        if(length1 == 0 and length2 == 0):
            return 0
        elif(length1 == 1 and length2 == 1):
            return int(word1[0] != word2[0])
        elif(length1 <= 1 or length2 <= 1):
            return abs(length1 - length2)

        for i in range(0,length1+1):
            output.append ([i] * (length2+1)) 
        for j in range (0, length2+1):
            output[0][j] = j 
        for i in range(1,length1 +1):
            for j in range (1, length2 + 1):
                if(word1[i-1] == word2[j-1]):
                    output[i][j] = output[i-1][j-1]
                else:
                    output[i][j] = min(output[i-1][j-1],output[i-1][j],output[i][j-1]) + 1
        return output[length1][length2]
                