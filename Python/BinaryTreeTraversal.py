# https://leetcode.com/problems/binary-tree-level-order-traversal/description/

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        previousLevel = 0
        output = []
        queue = []
        if(root is None):
            return output
        queue.append([root,0])
        currentLevelOutput = []
        while(len(queue) != 0):
            root = queue.pop(0)
            level = root[1]
            root = root[0]
            if(root.left != None):
                queue.append([root.left,level+1])
            if(root.right != None):
                queue.append([root.right,level+1])
            if(level != previousLevel):
                output.append(currentLevelOutput)
                currentLevelOutput = [root.val]
                previousLevel = level
            else:
                currentLevelOutput.append(root.val)
        if(len(currentLevelOutput) != 0):
            output.append(currentLevelOutput)
        return output
                