//https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/description/
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMin = function(nums) {
    let mid = 0
    let start = 0
    let end = nums.length-1
    while(start < end)
        {
        mid = Math.floor((start + end)/2)
        if (nums[start] < nums[end])
            return nums[start]
        else if(nums[mid] < nums[end])
            end = mid
        else
            start = mid + 1
        }
    return nums[start]
    };