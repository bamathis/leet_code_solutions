//https://leetcode.com/problems/add-two-numbers/description/
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2)
    {
    let carry = 0
    let leftCurr = l1
    let rightCurr = l2
    let length = Math.max(l1.length,l2.length)
    let output = null
    let outputCurr = output
    while(leftCurr!=null || rightCurr != null)
        {
        if(leftCurr != null)
            {
            firstVal = leftCurr.val
            leftCurr = leftCurr.next
            }
        else
            firstVal = 0
        if(rightCurr != null)
            {
            secondVal = rightCurr.val
            rightCurr = rightCurr.next
            }
        else
            secondVal = 0
        digit = firstVal + secondVal + carry
        if(digit >= 10)
            {
            digit = digit % 10
            carry = 1
            }
        else
            carry = 0
        if(outputCurr != null)
            {
            outputCurr.next = new ListNode(digit)
            outputCurr = outputCurr.next
            }
        else
            {
            output = new ListNode(digit)
            outputCurr = output
            }
        }
    if(carry === 1)
        outputCurr.next = new ListNode(carry)
    return output
    };