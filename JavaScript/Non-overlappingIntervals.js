//https://leetcode.com/problems/non-overlapping-intervals/description/
/**
 * Definition for an interval.
 * function Interval(start, end) {
 *     this.start = start;
 *     this.end = end;
 * }
 */
/**
 * @param {Interval[]} intervals
 * @return {number}
 */
var eraseOverlapIntervals = function(intervals)
    {
    let min = 0
    let lastAccepted = 0
    let sortedIntervals = mergeSort(intervals)
    for(let i = 1; i < sortedIntervals.length; i++)
        {
        if(sortedIntervals[i].start < sortedIntervals[lastAccepted].end)
            {
            min++
            }
        else
            lastAccepted = i
        }
    return min
    }

function mergeSort(array)
    {
    if(array.length < 2)
        {
        return array
        }
    else
        {
        let midPoint = Math.floor(array.length/2)
        let leftHalf = array.slice(0,midPoint)
        let rightHalf = array.slice(midPoint)
        return merge(mergeSort(leftHalf),mergeSort(rightHalf))
        }
    }

function merge(leftHalf, rightHalf)
    {
    let leftIndex = 0
    let rightIndex = 0
    let output = []
    while(leftIndex < leftHalf.length && rightIndex < rightHalf.length)
        {
        if(leftHalf[leftIndex].end < rightHalf[rightIndex].end)
            {
            output.push(leftHalf[leftIndex])
            leftIndex++
            }
        else
            {
            output.push(rightHalf[rightIndex])
            rightIndex++
            }
        }
    return output.concat(leftHalf.slice(leftIndex)).concat(rightHalf.slice(rightIndex))
    }